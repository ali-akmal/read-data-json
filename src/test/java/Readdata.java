
import java.lang.reflect.Array;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.*;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import javax.print.DocFlavor;

public class Readdata {

    public static void main(String[] args) {
        try {

            URL url = new URL("https://candidate.hubteam.com/candidateTest/v3/problem/dataset?userKey=e3937b5d6db0437b12ba20ff0528");

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.connect();

            //Getting the response code
            int responsecode = conn.getResponseCode();

            if (responsecode != 200) {
                throw new RuntimeException("HttpResponseCode: " + responsecode);
            } else {

                String inline = "";
                Scanner scanner = new Scanner(url.openStream());

                //Write all the JSON data into a string using a scanner
                while (scanner.hasNext()) {
                    inline += scanner.nextLine();
                }

                //Close the scanner
                scanner.close();

                //Using the JSON simple library parse the string into a json object
                JSONParser parse = new JSONParser();
                JSONObject data_obj = (JSONObject) parse.parse(inline);

                //Get the required object from the above created object
                //JSONObject obj = (JSONObject) data_obj.get("Global");

                //Get the required data using its key
               // System.out.println(obj.get("TotalRecovered"));

                JSONArray arr = (JSONArray) data_obj.get("partners");
                String pattern = "yyyy-MM-dd";
                SimpleDateFormat sDt = new SimpleDateFormat(pattern);

                for (int i = 0; i < arr.size(); i++) {
                    JSONObject new_obj = (JSONObject) arr.get(i);
                    List dates = (List) new_obj.get("availableDates");
                    for ( int ii = 0; ii < dates.size(); ii++){
                       String dat= (String) dates.get(ii);
                        DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd");
                        LocalDate date = LocalDate.parse(dat, format);
                        long diffInDays = ChronoUnit.DAYS.between(date, date1);


                        if (diffInDays==1){
                            System.out.print(date);
                            System.out.println(date1);
                        }
                        }



                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}